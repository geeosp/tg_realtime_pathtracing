﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSMain
//#pragma kernel CSPost
#pragma enable_d3d11_debug_symbols

//#include "..\Shaders\Structs.compute"

const float zeroCos = 0.00000000000001;
const float zeroDist = 0.0001;
const float maxFloatValue = 1; //vai ser sobrescrevido pela cpu


RWStructuredBuffer<int> debug;

//every struct size should be multiple of 4
struct BoxMinMax
{
    float4 min, max;
    int t_init, t_count;
    
};

struct Ray
{
    float4 origin;
    float4 dir;

};

struct CameraStruct
{
    float4 position;
    float4x4 baseTransform;
    float width, height, top, bottom, left, right, near, far;
    float4 backgroundColor;
};

struct MeshData
{
    int vertice_init; //o indice do primeiro vertice do mesh
    int vertice_count; //a quantidade de vertices do mesh
    int triangle_init; //o indice do primeiro triangulo do meh no buffer de triangulos
    int triangle_count; //a quantidade de triangulos
    int boundbox_init;
    int boundbox_count;
    int indice; //o indice no array de meshs
    int materialIndex; //indice do material
};

struct MeshTransform
{
    float4x4 localToWorld;
    float4x4 worldToLocal;
};


struct Hit
{
	
    float4 _point; //ponto de impacto
    float4 _normal; //normal do ponto
    float4 _color; //cor do ponto
    float _distance; //distancia do ponto para a origem do raio de teste
    int _modelIndex; // o indice do modelo

};


struct GeoMaterial
{
    float4 color;
    float diffuse;
    float specular;
    float transmitted;
    float emission;
    float specularCoef;
    float refractionCoeficient;
};



//initializa a buffer of this type for every pixel;
struct RayState
{
    uint type;
    uint2 pixel;
    float4 toLight;
    float4 viewer;
    Hit modelhit;
    float n_of_origin; //refraction coefitient of origin matter;
};

struct PixelStream
{
    uint2 pixel;
    float4 color;

};




// Create a RenderTexture with enableRandomWrite flag and set it
// with cs.SetTexture
RWTexture2D<float4> Result;

//RWTexture2D<float4> RandomTexture;
float INFINITY, PI;
float TIME, DELTA_TIME, RANDOM;
//uint rays_per_pixel;
uint raysDepth;
bool resetRender = false;

StructuredBuffer<CameraStruct> cameraBuffer;
StructuredBuffer<MeshData> bf_meshdata;
StructuredBuffer<MeshTransform> bf_meshtransform;
StructuredBuffer<float4> bf_vertices;
StructuredBuffer<int3> bf_triangles;
StructuredBuffer<float4> bf_normals;
StructuredBuffer<float4> bf_otherpoints;
StructuredBuffer<BoxMinMax> bf_boundingboxes;
StructuredBuffer<GeoMaterial> bf_materials;
StructuredBuffer<int> bf_lights;
StructuredBuffer<uint> bf_random_ints;
StructuredBuffer<float> bf_random_floats;
RWStructuredBuffer<uint> bf_random_index;

float4 ambient_color;
int meshCount, lightCount;
RWStructuredBuffer<int> hitStack;
RWStructuredBuffer<RayState> bf_rays;
//RWStructuredBuffer<Hit> bf_base_hits;
RWStructuredBuffer<int> bf_current_deep;

//int randomIndex;
float randomFloat(uint2 id)
{
    uint t_index = (cameraBuffer[0].width * id.x + id.y) % 1000;
    bf_random_index[t_index] = (bf_random_index[t_index] + 1) % 1000;
    return abs(sin(TIME * dot(1 + id.xy,
                         float2(DELTA_TIME, bf_random_floats[bf_random_index[t_index]]))));

    /*
    bf_random_index[t_index] = (bf_random_index[t_index] + 1) % 1000;
    
    return bf_random_floats[bf_random_index[t_index]];
    */

    /*
  randomIndex= (randomIndex+1)%1000;
  int nextIndex = bf_random_ints[randomIndex]%1000;
 
  return bf_random_floats[nextIndex];
  */

}
uint randomInt(uint2 id)
{
    return asuint(floor(maxFloatValue * randomFloat(id)));
 /*   
    int t_index = (cameraBuffer[0].width * id.x + id.y) % 1000;
    bf_random_index[t_index] = (bf_random_index[t_index] + 1) % 1000;
    return bf_random_ints[bf_random_index[t_index]];
    */
}

float4 randomVector(uint2 id)
{
    float4 v = float4(
   2.0* randomFloat(id)-1.0,
    2.0* randomFloat(id)-1.0,
  2.0*  randomFloat(id)-1.0,

    0);
    return normalize(v);
    //return normalize((2 * v) - 1);


}
//https://tavianator.com/fast-branchless-raybounding-box-intersections/
bool intersectRayAABB(BoxMinMax bx, Ray ray)
{


    float3 inverseray = float3(1.0 / ray.dir.x, 1.0 / ray.dir.y, 1.0 / ray.dir.z);

    double tx1 = (bx.min.x - ray.origin.x) * inverseray.x;
    double tx2 = (bx.max.x - ray.origin.x) * inverseray.x;

    double tmin = min(tx1, tx2);
    double tmax = max(tx1, tx2);

    double ty1 = (bx.min.y - ray.origin.y) * inverseray.y;
    double ty2 = (bx.max.y - ray.origin.y) * inverseray.y;

    tmin = max(tmin, min(ty1, ty2));
    tmax = min(tmax, max(ty1, ty2));


    double tz1 = (bx.min.z - ray.origin.z) * inverseray.z;
    double tz2 = (bx.max.z - ray.origin.z) * inverseray.z;

    tmin = max(tmin, min(tz1, tz2));
    tmax = min(tmax, max(tz1, tz2));



    return tmax >= tmin && tmax > 0;
    
    
    

}


float4 georeflect(float4 incident, float4 normal)
{
    incident = normalize(incident);
    normal = normalize(normal);
    //incident = normalize(incident);
    //normal = normalize(normal);
//    return incident - ( 2*dot(incident, normal) * normal);
  //  printf("oi");
    return incident - 2 * dot(incident, normal) * normal;

}
float4 geoProjection(float4 u, float4 v)
{
    return (dot(u, v) / dot(v, v)) * v;
    
}

bool georefract(float4 incident, float4 normal, float n1, float n2, out float4 refracted)
{ //http://sunandblackcat.com/tipFullView.php?l=eng&topicid=13
    /*
    incident = normalize(incident);
    normal = normalize(normal);
    float cos1 = dot(-incident, normal);
    float sin1 = sqrt(1 - cos1 * cos1);
    float sin2 = sin1 * n1 / n2;
    float cos2 = sqrt(1 - sin2 * sin2);
    float eta = n1 / n2;
    float k = (eta * cos1 - cos2);
    
    refracted = eta * incident + (eta * cos1 - cos2) * normal;
    if (dot(incident, refracted) < 0)
    {
        refracted = georeflect(incident, normal);
        return false;
    }

    return true;
    */
  
    /*
    // http://developer.download.nvidia.com/cg/refract.html
    float eta = n1 / n2;
    float cosi = dot(-incident, normal);
    float cost2 = 1.0f - eta * eta * (1.0f - cosi * cosi);
     refracted = eta * incident + ((eta * cosi - sqrt(abs(cost2))) * normal);
    if (cost2 > 0)
    {
        return true;

    }
    else
    {
        refracted = georeflect(incident, normal);
        return false;
    }
    */





  
    float4 proj = geoProjection(incident, normal);
    float4 k = incident - proj;
    double s1 = distance(k, 0);
    float s2 = s1 * n1 / n2;
    if (s2 <= 1)//refract
    {
        float c2 = sqrt(1 - s2 * s2);
        refracted = (s2 / s1) * k;
        refracted += c2 * normalize(proj);
        return true;
    }
    else
    {
        refracted = georeflect(incident, normal);
        return false;
    }
  
}

float3 coefbar(float4 p, float4 v0, float4 v1, float4 v2)
{

    float u, v;
    float4 v0v1, v0v2;
    v0v1 = v1 - v0;
    v0v2 = v2 - v0;
    float4 n = float4(0, 0, 0, 0);
    n.xyz = cross(v0v1.xyz, v0v2.xyz);
    float area2 = distance(float4(0, 0, 0, 0), n);
    (n);
    float4 e1 = v2 - v1;
    float4 vp1 = p - v1;
    float4 c = 0;
    c.xyz = cross(e1.xyz, vp1.xyz);

    u = distance(float4(0, 0, 0, 0), c) / area2;
    if (dot(n, c) < 0)
    {
        u = -u;
    }
    float4 e2 = v0 - v2;
    float4 vp2 = p - v2;
    c.xyz = cross(e2.xyz, vp2.xyz);
    v = distance(float4(0, 0, 0, 0), c) / area2;
    if (dot(n, c) < 0)
    {
        v = -v;
    }

    float3 ret;

    ret.x = 1 - u - v;
    ret.y = u;
    ret.z = v;
    return ret;
}

float getnorma(float4 v)
{
    return sqrt(dot(v, v));
}

Hit hitInBox(int modelIndex, Ray ray, int boxIndex)
{
    MeshData model = bf_meshdata[modelIndex];
    Hit hit;
    hit._color = 0;
    hit._modelIndex = -1;
    hit._normal = 0;
    hit._point = 0;
    Ray r;
    r.origin = mul(bf_meshtransform[model.indice].worldToLocal, ray.origin);
    r.dir = mul(bf_meshtransform[model.indice].worldToLocal, ray.dir);
    r.dir = normalize(r.dir);
    float minDist = INFINITY;
    BoxMinMax box = bf_boundingboxes[boxIndex];

    for (int t = model.triangle_init + box.t_init; t < model.triangle_init + box.t_init + box.t_count; t++)
    {
        float t_color = model.triangle_count;
        t_color = (t - model.triangle_init) / t_color;
        
        float4 p1 = float4(0, 0, 0, 1);
        float4 p2 = float4(0, 0, 0, 1);
        float4 p3 = float4(0, 0, 0, 1);
        float4 e1 = float4(0, 0, 0, 0);
        float4 e2 = float4(0, 0, 0, 0);
        float4 n = float4(0, 0, 0, 0);
        float4 p = float4(0, 0, 0, 0);
        

        p1.xyz = bf_vertices[bf_triangles[t].x].xyz;
        p2.xyz = bf_vertices[bf_triangles[t].y].xyz;
        p3.xyz = bf_vertices[bf_triangles[t].z].xyz;
        
        e1 = p2 - p1;
        e2 = p3 - p1;
        n.xyz = normalize(cross(e1.xyz, e2.xyz).xyz);

        n = normalize(n);
        //OLHA POR RAIOS PARALELOS
        if (abs(dot(n, n) - dot(n, r.dir)) > zeroCos)
        {
            float4 p1p0 = r.origin - p1;
            float s = -dot(n, p1p0) / dot(n, r.dir);
            if (s > zeroDist)//verify if point is different from origin
            {
                p = r.origin + mul(s, r.dir);
                bool ok = true;
                float3 coef;
                coef = coefbar(p, p1, p2, p3);
                //verify if point is inside triangle

                for (int i = 0; i < 3; i++)
                {
                    if (coef[i] < 0 || coef[i] > 1)
                    {
                        ok = false;
                    }
                }

                if (ok)//nice, point is in triangle
                {
                    float dist = getnorma(mul(bf_meshtransform[model.indice].localToWorld, p) - ray.origin);
                    if (dist < minDist)
                    {
                        minDist = dist;

                        float4 n0 = 0, n1 = 0, n2 = 0, n3 = 0;
                       
                        n1.xyz = bf_normals[bf_triangles[t].x].xyz;
                        n2.xyz = bf_normals[bf_triangles[t].y].xyz;
                        n3.xyz = bf_normals[bf_triangles[t].z].xyz;
                                              

                        n0 = coef.x * n3 + coef.y * n1 + coef.z * n2;
                        n = n0;
                           
                        if (dot(n, r.dir) > 0)
                        {
                            n = -n;
                        }
                        n.w = 0;
                        n = normalize(n);
                        //transformando para o espaço de mundo agora
                        hit._point = mul(bf_meshtransform[model.indice].localToWorld, p);
                        hit._color = bf_materials[model.materialIndex].color;
                        hit._color.w = 1;
                        hit._normal = mul(bf_meshtransform[model.indice].localToWorld, n);
                        hit._distance = getnorma(mul(bf_meshtransform[model.indice].localToWorld, p - r.origin));
                        hit._modelIndex = model.indice;
                    }
                }
            }
        }
    }
    return hit;
}












//Ray r should be in world space;
//lastHit é o melhor hit ja encontrado ate agora
Hit GetNearestHit(uint stackInit, uint boxIndex3, Ray r)
{
    Hit lastHit;
    lastHit._distance = maxFloatValue;
    lastHit._color = float4(0, 0, 0, 0);
    lastHit._modelIndex = -1;
    for (int meshIndex = 0; meshIndex < meshCount; meshIndex++)
    {
        
        MeshData md = bf_meshdata[meshIndex];
        MeshTransform mf = bf_meshtransform[meshIndex];
        
        //ray in bounding box localspace
        Ray ray;
        ray.origin = mul(mf.worldToLocal, r.origin);
        ray.dir = mul(mf.worldToLocal, r.dir);
        //se intersectar
        //
        uint stackIndex = 0;

        hitStack[stackInit + stackIndex] = boxIndex3;
        stackIndex = 1;

        while (stackIndex != 0)
        {
            // ---- stack pop
            stackIndex--;
            int boxIndex = hitStack[stackInit + stackIndex];
            //----- stack pop
            
            BoxMinMax currentBox = bf_boundingboxes[md.boundbox_init + boxIndex];
            
            if (intersectRayAABB(currentBox, ray))
            {
                if (boxIndex < md.boundbox_count / 2 - 1)
                {


                    hitStack[stackInit + stackIndex] = 2 * boxIndex + 1;
                    stackIndex++;
                    hitStack[stackInit + stackIndex] = 2 * boxIndex + 2;
                    stackIndex++;

                }
                else //é uma folha
                {
                    //   lastHit._color = float4(1, 0, 1, 0);
                    Hit h;
                    h = hitInBox
                    (meshIndex, r, md.boundbox_init + boxIndex);

                    if (h._modelIndex >= 0 && h._distance < lastHit._distance)
                    {
                        lastHit = h;
                    }
                }
                
            }
        }

    }
    lastHit._color.w = 1;
    return lastHit;
}



float4 getPointForLight(uint2 id, uint lightIndex)
{
    MeshData md = bf_meshdata[bf_lights[lightIndex]];
    MeshTransform mt = bf_meshtransform[bf_lights[lightIndex]];
    uint randTriangle = randomInt(id) % md.triangle_count;
    float r1 = randomFloat(id);
    float r2 = randomFloat(id) * (1 - r1);
    float r3 = 1 - r2 - r1;
    float4 v = r1 * (bf_vertices[bf_triangles[md.triangle_init + randTriangle].x]);
    v += r2 * (bf_vertices[bf_triangles[md.triangle_init + randTriangle].y]);
    v += r3 * (bf_vertices[bf_triangles[md.triangle_init + randTriangle].z]);




   // return mul(mt.localToWorld, bf_vertices[md.vertice_init + randTriangle % md.vertice_count]);
    return mul(mt.localToWorld, v);
    //return v;

}

float4 phongColor2(uint2 id, uint pxInd, Hit lastHit, float4 origin)
{
    float4 color = 0, ambient = 0, diffuse = 0, specular = 0;
    if (lastHit._modelIndex >= 0)
    {
        GeoMaterial mat = bf_materials[bf_meshdata[lastHit._modelIndex].materialIndex];
        if (mat.emission > 0)
        {
            color = mat.color * mat.emission;
        }
        else
        {

            int li = randomInt(id.xy) % lightCount;
            int lightmodelIndex = bf_lights[li];
            GeoMaterial lightMat = bf_materials[bf_meshdata[bf_lights[li]].materialIndex];
            float4 toViewer = origin - lastHit._point;
            Ray rlight;
            rlight.dir = -lastHit._normal;
            for (uint t = 0; t < 4 && dot(rlight.dir, lastHit._normal) <= 0; t++)
            {
                float4 lightPoint = getPointForLight(id.xy, li);
                rlight.dir = normalize(lightPoint - lastHit._point);
                rlight.origin = lastHit._point + .001 * rlight.dir;

            }
       
            Hit lightHit = GetNearestHit(64 * pxInd, 0, rlight);

            float decay = 1;
            float4 ambiance = 00, diffuse = 0, specular = 0;
            ambiance = ambient_color * mat.color;
            if (lightHit._modelIndex == lightmodelIndex)
            {
                float d = getnorma(lightHit._point - lastHit._point);
                decay = 1 / (d);
                float4 toLight = rlight.dir;
                float4 normal = lastHit._normal;
                float4 toViewer = normalize(origin - lastHit._point);
                float cosLN = dot(toLight, normal);
                float4 lightIntensity = lightMat.color * lightMat.emission;
                if (cosLN >= 0)
                {
                    diffuse = (cosLN * mat.diffuse) * (mat.color * lightIntensity);
                }
                float4 reflected = georeflect(-toViewer, normal);
              
                float cosRL = dot(reflected, toLight);
                if (cosRL > 0)
                {
                    specular = mat.specular * pow(cosRL, mat.specularCoef) * (lightIntensity * mat.color);
                }

               
            }
            color = ambiance + decay * (diffuse + specular);
           // color *= decay;
            



        }
        
    }
    
    color.w = 1;
    return color;
    
}
/*

float4 phongColor(uint2 id, uint pxInd, Hit lastHit, float4 origin)
{
    
    float4 color = 0, ambient = 0, diffuse = 0, specular = 0;
    
    float decay = 1;
    if (lastHit._modelIndex >= 0)//bateu em alguma coisa
    {
            
        GeoMaterial mat = bf_materials[bf_meshdata[lastHit._modelIndex].materialIndex];
        if (mat.emission > 0)//isLight
        {
            decay = 1 / distance(lastHit._point, origin);
            diffuse = mat.color * (mat.emission);
        }
        else //is not light
        {
            ambient = mat.color * ambient_color;
         
            
            Ray tolight;
          //  tolight.dir = -2 * lastHit._normal;
            int li = randomInt(id.xy) % lightCount;
           // int li = 0;
            for (uint t = 0; t < 4 && dot(tolight.dir, lastHit._normal) <= 0; t++)
            {
                float4 lightPoint = getPointForLight(id.xy, li);
                tolight.dir = normalize(lightPoint - lastHit._point);
                tolight.origin = lastHit._point + .001 * tolight.dir;

            }
            if (dot(tolight.dir, lastHit._normal) >= 0)
            {
                Hit lightHit = GetNearestHit(64 * pxInd, 0, tolight);
                decay = decay / distance(lastHit._point, lightHit._point);
                if (lightHit._modelIndex == bf_lights[li])
                { //acertou a luz
                    GeoMaterial lightMat = bf_materials[bf_meshdata[bf_lights[li]].materialIndex];
                    float cosLN = dot(lastHit._normal, tolight.dir);
                    if (cosLN > 0)
                    {
                        diffuse = lightMat.emission * (mat.color * lightMat.color) * cosLN;
                    }
                    float4 fromOrigin = normalize(origin - lastHit._point);
                    float4 reflected = normalize(georeflect(-tolight.dir, lastHit._normal));
                    float cosRO = dot(fromOrigin, reflected);
                    if (cosRO > 0)
                    {
                        specular = pow(cosRO, mat.specularCoef) * lightMat.color * lightMat.emission;
                    }
                }
            }
        }
        float total = mat.diffuse + mat.specular;
        color = ambient + (decay / total) * (diffuse * mat.diffuse + specular * mat.specular);
    }
    else
    {
        color = cameraBuffer[0].backgroundColor;

    }
    
    
    color.w = 1;
    return color;


}
*/
void updateColor(uint2 id, float4 color)
{
    float4 ic = Result[id.xy];
    ic.xyz = ic.xyz * ic.w;
   // color.w = 1;
    ic += color;
    if (ic.w > 0)
    {
        ic.xyz = ic.xyz / ic.w;
    }
    Result[id.xy] = ic;
    
}

void preparePath(uint2 id, bool override)
{
    CameraStruct cam = cameraBuffer[0];
    uint pxInd = id.x * cam.width + id.y;
    float y = (id.y / cam.height) * (cam.top - cam.bottom) + cam.bottom;
    float z = 1;
    float x = (id.x / cam.width) * (cam.right - cam.left) + cam.left;
    float4 dir = float4(x, y, 1, 0);
    Hit lastHit;
   
    Ray r;
    r.origin = cam.position;
    r.dir = normalize(mul(cam.baseTransform, dir));
    lastHit = GetNearestHit(64 * pxInd, 0, r);
    lastHit._color = phongColor2(id.xy, pxInd, lastHit, r.origin);
        
    RayState rayState;
    rayState.pixel = id.xy;
    rayState.modelhit = lastHit;
    rayState.viewer = r.origin;
    rayState.type = 0;
    rayState.toLight = 0;
    rayState.n_of_origin = 1; //default n  of air;
    if (override)
    {
        updateColor(id, lastHit._color);
    }
    bf_rays[pxInd * raysDepth] = rayState;
  
}


float4 calculatePath(uint2 id, uint pxlInd, uint bf_rays_ind)
{
    const uint currdepp = debug[pxlInd];
    float4 currentColor = bf_rays[bf_rays_ind + currdepp].modelhit._color;
    for (int i = currdepp - 1; i >= 0; i--)
    {
        RayState currState = bf_rays[bf_rays_ind + i];
        RayState nexState = bf_rays[bf_rays_ind + i + 1];
        GeoMaterial mat = bf_materials[bf_meshdata[currState.modelhit._modelIndex].materialIndex];
        float decay = 1.0;
        //decay/= (distance(currState.modelhit._point, nexState.modelhit._distance));
        
		if (currState.type == 0)
        {
            float cosNL = dot(currState.modelhit._normal, currState.toLight);
            currentColor = currState.modelhit._color + decay*cosNL * (mat.color * currentColor);

        }
        else if (currState.type == 1)
        {
          //  GeoMaterial mat = bf_materials[bf_meshdata[currState.modelhit._modelIndex].materialIndex];
            float toviewer = normalize(currState.viewer - currState.modelhit._point);
            float tolight = currState.toLight;
            float reflected = georeflect(-toviewer, currState.modelhit._normal);
            float cosRL = dot(reflected, tolight);
            currentColor = currState.modelhit._color +decay* (mat.color * currentColor);

        }
        else if (currState.type == 2)
        {
            //currentColor = min(mat.color, currentColor);
           currentColor =decay* mat.color * currentColor;

        }

    }




    return currentColor;
}

void tracePath2(uint2 id, uint pxlInd, uint bf_rays_ind)
{ //assumimos que o state que estiver nesse deep ja teve seu hit calculado.
    bool shouldCalculate = false;
    const uint deep = debug[pxlInd];
    RayState currState = bf_rays[bf_rays_ind + deep];
    currState.modelhit._color = phongColor2(id, pxlInd, currState.modelhit, currState.viewer);
    if (deep == raysDepth - 1)
    {
        shouldCalculate = true;
    }
    else
    {
        if (currState.modelhit._modelIndex < 0)//nao aeertou em nada
        {
            shouldCalculate = true;
        }
        else
        {
            GeoMaterial mat = bf_materials[bf_meshdata[currState.modelhit._modelIndex].materialIndex];
            if (mat.emission > 0)// é uma luz
            {
                shouldCalculate = true;
            }
            else
            { //estamos em um hit que nao é o ultimo
                RayState nexState;
                nexState.n_of_origin = currState.n_of_origin;
                float4 nexdir = 0;
                float t = (mat.specular + mat.diffuse + mat.transmitted) * randomFloat(id);
                if (t < mat.diffuse)
                {
                    nexdir = randomVector(id);
                    if (dot(nexdir, currState.modelhit._normal) < 0)
                    {
                        nexdir = -nexdir;
                    }
                    currState.type = 0;
                }
                else if (t < mat.diffuse + mat.specular)
                {
                    float4 toViewer = normalize(currState.viewer - currState.modelhit._point);
                    nexdir = georeflect(-toViewer, currState.modelhit._normal);
                    nexdir = normalize(nexdir + randomVector(id) * min(1, pow(.5, mat.specularCoef)));
                    currState.type = 1;
                }
                else if (t < mat.diffuse + mat.transmitted + mat.specular)
                {
                    float4 toViewer = normalize(currState.viewer - currState.modelhit._point);
                    float n1 = currState.n_of_origin;
                    float n2 = mat.refractionCoeficient;
                    if (n1 == n2)
                    {
                        n2 = 1;
                    }
                    nexdir = refract(-toViewer, currState.modelhit._normal, n1 / n2);
             
                    if (georefract(-toViewer, currState.modelhit._normal, n1, n2, nexdir))
                    {
                        nexState.n_of_origin = mat.refractionCoeficient;
                        currState.type = 2;

                    }
                    else
                    {
                        //nexdir is a reflection
                        currState.type = 2  ;
                        nexState.n_of_origin = currState.n_of_origin;
                        

                    }
                    
                 //  currState.type = 2;
                   // nexState.n_of_origin = mat.refractionCoeficient;
                }

                currState.toLight = nexdir;
                

                Ray r;
                r.dir = nexdir;
                r.origin = currState.modelhit._point + .001 * r.dir;
                nexState.modelhit = GetNearestHit(64 * pxlInd, 0, r);
                nexState.toLight = 0; //will be overrided;
                nexState.type = -1; //will be overrided;
                nexState.pixel = id;
                nexState.viewer = r.origin;
                


                bf_rays[bf_rays_ind + deep] = currState; //atualizando alteracoes no currstate;
                bf_rays[bf_rays_ind + deep + 1] = nexState; //adicionando o nexState na pilha;
                debug[pxlInd] = deep + 1; //avisando que a pilha aumentou um agora.
            }
        }
    }
    
    if (shouldCalculate)
    {
        float4 pathColor = calculatePath(id, pxlInd, bf_rays_ind);
      //  pathColor.w = 1;
        updateColor(id, pathColor);
        debug[pxlInd] = 0; //reseta a pilha
    }
    
}
float tonemapping;
int currPixelIndex;
[numthreads(8, 8, 1)]

    void CSMain
    (
    uint3 id : SV_DispatchThreadID)
{

    CameraStruct cam = cameraBuffer[0];
    uint pxInd = id.x * cam.width + id.y;
    uint bf_rays_ind = pxInd * raysDepth;
    if (resetRender)
    {
        Result[id.xy] = float4(0, 0, 0, 0); //black
        debug[pxInd] = 0;
        preparePath(id.xy, true);
      
    }
    else
    {
        tracePath2(id.xy, pxInd, bf_rays_ind);
    }

   // Result[id.xy] =.5+.5*randomVector(id.xy) ;
}