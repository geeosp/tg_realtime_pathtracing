﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickMovement : MonoBehaviour
{

    public float minY = 0, maxY = 3;
    public float maxVeocity = .1f;
    public Vector3 velocity = Vector3.zero;
    public Vector3 rotationVelocity = new Vector3(10, 10, 0);
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float thershold = .01f;
        if (Mathf.Abs(Input.GetAxis("Horizontal") )> thershold || Mathf.Abs(Input.GetAxis("Vertical")) > thershold)
        {

            Vector3 position = transform.position + Input.GetAxis("Horizontal") * maxVeocity * transform.right + Input.GetAxis("Vertical") * maxVeocity * transform.forward;
            position.y = Mathf.Min(maxY, Mathf.Max(minY, position.y));
            transform.position = position;
        }
        if (Mathf.Abs(Input.GetAxis("Vertical2")) > thershold|| Mathf.Abs(Input.GetAxis("Horizontal2")) > thershold)
        {
            Quaternion rot;
            rot = transform.rotation;

            rot.eulerAngles = rot.eulerAngles + 5 * Time.deltaTime * new Vector3(Input.GetAxis("Vertical2") * rotationVelocity.x, Input.GetAxis("Horizontal2") * rotationVelocity.y, 0);
            transform.rotation = rot;
        }
    }
}
