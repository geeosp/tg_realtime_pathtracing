﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;






public class TesteScript : MonoBehaviour
{
    /*
    [System.Serializable]
    public struct BoundBx
    {
        public Vector4 center;
        public Vector4 bounds;
    }
    [System.Serializable]
    public struct CameraStruct
    {
        public Vector4 origin;
        public Matrix4x4 baseTransform;
    }
    public ComputeShader shader;
    public ComputeBuffer bbxBuffer;
    public ComputeBuffer cameraBuffer;

    public float top, bottom, left, right, near, far;
    int kernelHandle;

    public GeoMesh[] geomeshs;
    BoundBx[] boundingBx;
    [SerializeField]
    public BoundBx[] result;
    void Start()
    {
        kernelHandle = shader.FindKernel("CSMain");
        shader.SetFloat("INFINITY", float.MaxValue);

    }

    // Update is called once per frame
    void Update()
    {


        geomeshs = FindObjectsOfType<GeoMesh>();
        if (bbxBuffer != null)
        {
            bbxBuffer.Dispose();
        }
        boundingBx = new BoundBx[geomeshs.Length + geomeshs.Length % 2];//meshcount tem que ser par
        bbxBuffer = new ComputeBuffer(boundingBx.Length, 8 * (4 + 4), ComputeBufferType.Counter);
        shader.SetInt("meshCount", (int)geomeshs.Length);
        shader.SetBuffer(kernelHandle, "bbxBuffer", bbxBuffer);
    }

    public uint count;
    void ComputeRender(RenderTexture tex, int width, int height)
    {
        shader.SetFloat("INFINITY", float.MaxValue);
        shader.SetFloat("PI", Mathf.PI);
       




        for (int i = 0; i < geomeshs.Length; i++)
        {
            GeoMesh g = geomeshs[i];
            List<Mesh> meshs = new List<Mesh>();
            meshs.Add(g.getMesh());
            boundingBx[i] = new BoundBx();
          //  boundingBx[i].center = g.bound_min;
        //    boundingBx[i].bounds = g.boundmax;

        }



        bbxBuffer.SetData(boundingBx);

        result = new BoundBx[boundingBx.Length];
        shader.SetTexture(kernelHandle, "Result", tex);




        Camera camera = GetComponent<Camera>();
        near = camera.nearClipPlane; far = camera.farClipPlane;

        top = Mathf.Tan((camera.fieldOfView * .5f) * Mathf.PI / 180f);//top com z = 1;
        if (top < 0) top *= -1;
        bottom = -top; // bottom com z= 1;
        right = top * camera.aspect;
        left = -right;

        shader.SetFloat("width", width);
        shader.SetFloat("height", height);
        shader.SetFloat("left", left);
        shader.SetFloat("right", right);
        shader.SetFloat("top", top);
        shader.SetFloat("bottom", bottom);
        shader.SetFloat("near", near);
        shader.SetFloat("far", far);
        Vector4 cameraPos = camera.transform.position;
        cameraPos.w = 1;




        Camera[] cameras = FindObjectsOfType<Camera>();
        cameraPos = cameras[0].transform.position;
        cameraPos.w = 1;

        if (cameraBuffer != null)
        {
            cameraBuffer.Dispose();
        }
        cameraBuffer = new ComputeBuffer(2, 8 * (4 + 4 * 4));
        CameraStruct c = new CameraStruct();
        c.origin = cameraPos;
        c.baseTransform = Matrix4x4.TRS(Vector3.zero, cameras[0].transform.rotation, new Vector3(1, 1, 1));
        CameraStruct[] structs = new CameraStruct[2];
        structs[0] = c;
        structs[1] = c;
        cameraBuffer.SetData(structs);
        shader.SetBuffer(kernelHandle, "cameraBuffer", cameraBuffer);



        shader.SetVector("camerapos", cameraPos);
        shader.Dispatch(kernelHandle, width / 8, height / 8, 1);

        bbxBuffer.GetData(result, 0, 0, result.Length);

        count = (uint)result.Length;

    }

    public RenderTexture renderTexture;
    public int lastWidht = 0, lastHeight = 0;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (renderTexture == null || source.width != lastWidht || source.height != lastHeight)
        {
            lastWidht = source.width;
            lastHeight = source.height;
            if (renderTexture != null)
            {
                renderTexture.DiscardContents();
                renderTexture.Release();
            }
            renderTexture = new RenderTexture(source);
            renderTexture.enableRandomWrite = true;

            renderTexture.Create();
        }
        if (renderTexture.IsCreated())
        {
            ComputeRender(renderTexture, lastWidht, lastHeight);
            Graphics.Blit(renderTexture, destination);
        }


    }



    private void OnDestroy()
    {
        bbxBuffer.Dispose();
        cameraBuffer.Dispose();
    }


    */
}
