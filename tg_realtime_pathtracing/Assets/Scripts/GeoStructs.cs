﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoStructs
{
    /*
      //Structs
      [System.Serializable]
      public struct BoundBx
      {
          public Vector4 min;
          public Vector4 max;
          //size  = 4*(4+4)
      }
      */

    [System.Serializable]
    public struct BoxMinMax
    {
        public Vector4 min, max;
        public int t_init, t_count;
        public BoxMinMax(int zero)
        {
            t_init = 0;
            t_count = 0;
            min = Vector4.positiveInfinity;
            max = Vector4.negativeInfinity;
        }
    }

    [System.Serializable]
    public struct CameraStruct
    {
        public Vector4 position; // position of camera
        public Matrix4x4 baseTransform; // matriz dde transformacao de base
        public float width, height, top, bottom, left, right, near, far;
        public Vector4 backgroundColor;
        //size = 4*(4+16+8)=
    }

    [System.Serializable]
    //COLD
    public struct MeshData
    {
        public int vertice_init;//o indice do primeiro vertice pertencente ao mesh //uvs
        public int vertice_count;//o numero de vertices do objeto 
        public int triangle_init;//o indice do primeiro triangulo do objeto;//tambem vale para as normais 
        public int triangle_count;//o numero de triangulos;//tambem vale para as normais;
        public int boundbox_init;
        public int boundbox_count;
        public int indice;//indice no array de meshs;
        public int materialIndex;//indice do material
        //size = 4+4+4+4+4+4 =  24 bytes;
    }
    //HOT
    [System.Serializable]
    public struct MeshTransform
    {
        public Matrix4x4 localToWorld; //matrix de mudanca de base, do mesh local para o muno real. contem informacoes de posicao, tamanho e rotacao
        public Matrix4x4 worldToLocal;
        //size = 4*4*4+4*4*4 = 64 bytes;
    }
    //nao existe um Vector equivalente a int3 do shader
    [System.Serializable]
    public struct IntVector3
    {
        public int x, y, z;
        public IntVector3(int[] xyz)
        {
            x = xyz[0];
            y = xyz[1];
            z = xyz[2];
        }
    }

    [System.Serializable]
    public struct GeoMaterial
    {
        public Vector4 color;
        public float diffuse;
        public float specular;
        public float transmitted;
        public float emission;
        public float specularCoef;
        public float refractionCoeficient;
    }

    public struct Hit
    {

        Vector4 _point; //ponto de impacto
        Vector4 _normal; //normal do ponto
        Vector4 _color; //cor do ponto
        float _distance; //distancia do ponto para a origem do raio de teste
        int _modelIndex; // o indice do modelo

    };

    [System.Serializable]
    public struct RayState
    {
        uint type;//0 = diffuse, 1= reflect, 2=transmitted
        uint pixelx, pixely;
        Vector4  toLight;
        Vector4 fromOrigin;
        Hit modelhit;
        float n_of_origin;//refraction coeficient from origin matter.
    }
    [System.Serializable]
    public struct PixelStream{
        uint pixelx;
        uint pixely;
        Vector4 color;
    }
}
