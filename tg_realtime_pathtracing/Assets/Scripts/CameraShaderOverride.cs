﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaderOverride : MonoBehaviour {
public Shader shaderToOverride;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Camera>().RenderWithShader(shaderToOverride, "RenderType");
		
	}
}
