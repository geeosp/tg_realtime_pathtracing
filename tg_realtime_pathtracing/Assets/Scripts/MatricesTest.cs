﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**Classe para testar algumas funcoes de matrizes
 * 
 * 
 */
public class MatricesTest : MonoBehaviour {


   public  Matrix4x4 matrix;
    public Vector4 myPoint;
    public Vector4 resultPoint;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        resultPoint = matrix.MultiplyPoint(myPoint);
	}
}
