﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GeoStructs;


[RequireComponent(typeof(Camera))]
public class GeoRenderer : MonoBehaviour, System.IDisposable
{
    /*
     strides sizes
     1 float  = 4;
     1 int = 4;
     i double = 8
     vector4 = 4*4
     matrix 4x4 = 4*4*4


    */











    //identificador da funcao csmain no shader
    int kernelHandle;
    //o compute shader que a gente vai usar
    public ComputeShader shader;
    //buffers
    //cada buffer é uma representaçao dos buffers presentes na gpu

    //pra passar as informaçoes de camera, na verdade, so vai ter uma
    ComputeBuffer bf_camera;
    public int raysDepth = 8;
    //initialize once and everytime a mesh is added/changed/deleted;

    ComputeBuffer bf_meshdata;
    ComputeBuffer bf_vertices;
    ComputeBuffer bf_triangles;
    ComputeBuffer bf_normals;
    ComputeBuffer bf_boundingboxes;
    //pass to computeShader everyTime a mesh Changed;
    ComputeBuffer bf_meshtransform;
    ComputeBuffer hitStack;

    ComputeBuffer bf_materials;
    ComputeBuffer bf_lights;
    ComputeBuffer bf_random_ints;
    ComputeBuffer bf_random_floats;
    ComputeBuffer bf_rays;

    ComputeBuffer bf_current_deep;
    //dataArrays, one for each buffer
    GeoMesh[] meshs;
    CameraStruct[] cameraData;



    //tests
     float tonemapping;
    List<Vector4> vertexArray = new List<Vector4>();
    List<Vector4> normalArray = new List<Vector4>();
    List<IntVector3> triangleArray = new List<IntVector3>();
     List<MeshTransform> meshTransformArray = new List<MeshTransform>();
     List<MeshData> meshDataArray = new List<MeshData>();
    List<BoxMinMax> boundingBoxArray = new List<BoxMinMax>();
    public Color ambientColor;
    int[] currentDeeps;


    private static GeoRenderer instance;
    public static GeoRenderer getInstance()
    {
        // if (instance != null)
        return instance;
    }
    // Use this for initialization
    ComputeBuffer debug;
     int[] debugData = new int[50];
    void Start()
    {
        if (instance == null)
        {
            // DontDestroyOnLoad(this);
            instance = this;
            //renderer initialization

            kernelHandle = shader.FindKernel("CSMain");
            shader.SetFloat("INFINITY", float.MaxValue);
            shader.SetFloat("INFINITY", float.MaxValue);
            shader.SetFloat("PI", Mathf.PI);

            lastCameraPos = transform;
            Camera camera = GetComponent<Camera>();
            camera.cullingMask = 0;

            UpdateMeshs();
            updateRandoms();

            globalSize = sizeof(float);

        }
        else
        {
            Destroy(this);
        }
    }
    void FixedUpdate()
    {
        // Camera camera= GetComponent<Camera>();
        ;// shader.SetVector("background_color", new Vector4(camera.backgroundColor.r, camera.backgroundColor.g, camera.backgroundColor.b, camera.backgroundColor.a));
         // print("fixed update");
    }
    void updateRandoms()
    {
        List<uint> listInt = new List<uint>();
        List<float> listFloat = new List<float>();

        for (int i = 0; i < 1000; i++)
        {
            listFloat.Add(Random.value);
            listInt.Add((uint)Random.value * uint.MaxValue);
        }
        destroyBuffer(bf_random_ints);
        destroyBuffer(bf_random_floats);
        fromListToBuffer(ref listInt, ref bf_random_ints);
        fromListToBuffer(ref listFloat, ref bf_random_floats);
        shader.SetBuffer(kernelHandle, "bf_random_ints", bf_random_ints);
        shader.SetBuffer(kernelHandle, "bf_random_floats", bf_random_floats);
        shader.SetBuffer(kernelHandle, "bf_random_index", bf_random_ints);


    }


    // Update is called once per frame
    void Update()
    {

        shader.SetFloat("TIME", Time.time);
        shader.SetFloat("DELTA_TIME", Time.deltaTime);
        shader.SetFloat("RANDOM", Random.value);
        shader.SetFloat("tonemapping", tonemapping);
        shader.SetFloat("maxFloatValue", float.MaxValue);

        shader.SetInt("raysDepth", raysDepth);
        shader.SetVector("ambient_color", new Vector4(ambientColor.r, ambientColor.g, ambientColor.b, 1));
        //      shader.SetInts("RANDOM_INTS", randomInts);


        if (bf_current_deep != null && currentDeeps != null)
        {
            //  bf_current_deep.GetData(currentDeeps);
        }
        UpdateMeshsTransforms();


    }
     int globalSize;

    void ComputeRender(RenderTexture tex, int width, int height)
    {
        if (UpdateCamera(width, height))
        {
            ResetRender();
        }
        shader.SetBool("resetRender", resetRender);

        shader.Dispatch(kernelHandle, width / 8, height / 8, 1);
        // \\ y 6  shader.Dispatch(lonekernel, 1, 1, 1);
        // int lonekernel = shader.FindKernel("CSPost");
        debug.GetData(debugData);
        resetRender = false;




    }

    public void UpdateMeshsTransforms()
    {
        meshTransformArray = new List<MeshTransform>();

        List<GeoMaterial> materials = new List<GeoMaterial>();
        for (int m = 0; m < meshs.Length; m++)
        {
            GeoMesh g = meshs[m];

            MeshTransform meshTransform = new MeshTransform();

            meshTransform.localToWorld = g.transform.localToWorldMatrix;
            meshTransform.worldToLocal = g.transform.worldToLocalMatrix;
            meshTransformArray.Add(meshTransform);
            materials.Add(g.material);

        }
        fromListToBuffer<MeshTransform>(ref meshTransformArray, ref bf_meshtransform);
        shader.SetBuffer(kernelHandle, "bf_meshtransform", bf_meshtransform);
        if (bf_materials != null)
            bf_materials.SetData(materials.ToArray());
    }

    public void UpdateMeshs()
    {
        meshs = FindObjectsOfType<GeoMesh>();
        int meshCount = meshs.Length;

        vertexArray = new List<Vector4>();
        triangleArray = new List<IntVector3>();
        List<GeoMaterial> materials = new List<GeoMaterial>();
        List<int> lights = new List<int>();
        for (int m = 0; m < meshs.Length; m++)
        {
            GeoMesh g = meshs[m];
            g.modelIndex = m;
            g.generateBoundingBoxes();
            MeshData data = new MeshData();
            MeshTransform meshTransform = new MeshTransform();
            Mesh ms = g.getMesh();
            //initialize meshData
            data.indice = m;
            data.vertice_init = vertexArray.Count;
            data.vertice_count = ms.vertexCount;
            data.triangle_init = triangleArray.Count;
            data.triangle_count = ms.triangles.Length / 3;
            data.boundbox_init = boundingBoxArray.Count;
            data.boundbox_count = g.getBoundBoxTree().Length;
            data.materialIndex = materials.Count;
            materials.Add(g.material);
            //    data.color = g.color;
            //initialize meshbuffers
            for (int i = 0; i < data.vertice_count; i++)
            {
                Vector4 v = ms.vertices[i];
                v.w = 1;
                vertexArray.Add(v);
                if (ms.normals.Length == ms.vertices.Length)
                {
                    Vector4 normal = ms.normals[i];
                    normalArray.Add(normal);

                }

            }

            for (int i = 0; i < data.triangle_count; i++)
            {
                triangleArray.Add(new IntVector3(new int[] {
                    ms.triangles[3*i]+data.vertice_init,
                    ms.triangles[3*i+1]+data.vertice_init,
                    ms.triangles[3*i+2]+data.vertice_init
                }));

            }
            for (int i = 0; i < g.getBoundBoxTree().Length; i++)
            {
                boundingBoxArray.Add(g.getBoundBoxTree()[i]);
            }

            //inicializou todos os elementos do data;
            meshDataArray.Add(data);
            meshTransformArray.Add(meshTransform);
            g.meshData = data;
            g.meshTransform = meshTransform;
            if (g.isLight())
            {
                lights.Add(data.indice);
            }


        }
        UpdateMeshsTransforms();
        fromListToBuffer<MeshData>(ref meshDataArray, ref bf_meshdata);
        fromListToBuffer<MeshTransform>(ref meshTransformArray, ref bf_meshtransform);
        fromListToBuffer<Vector4>(ref vertexArray, ref bf_vertices);
        fromListToBuffer<Vector4>(ref normalArray, ref bf_normals);
        fromListToBuffer<IntVector3>(ref triangleArray, ref bf_triangles);
        fromListToBuffer<BoxMinMax>(ref boundingBoxArray, ref bf_boundingboxes);
        fromListToBuffer<GeoMaterial>(ref materials, ref bf_materials);
        fromListToBuffer<int>(ref lights, ref bf_lights);
        shader.SetInt("meshCount", meshCount);
        shader.SetInt("lightCount", lights.Count);
        //   print(lights.Count);
        shader.SetBuffer(kernelHandle, "bf_meshdata", bf_meshdata);
        shader.SetBuffer(kernelHandle, "bf_meshtransform", bf_meshtransform);
        shader.SetBuffer(kernelHandle, "bf_vertices", bf_vertices);
        shader.SetBuffer(kernelHandle, "bf_triangles", bf_triangles);
        shader.SetBuffer(kernelHandle, "bf_normals", bf_normals);
        shader.SetBuffer(kernelHandle, "bf_boundingboxes", bf_boundingboxes);
        shader.SetBuffer(kernelHandle, "bf_materials", bf_materials);
        shader.SetBuffer(kernelHandle, "bf_lights", bf_lights);

















    }



    private void OnDestroy()
    {
        Dispose();

    }
    private void destroyBuffer(ComputeBuffer buffer)
    {
        if (buffer != null)
        {
            buffer.Dispose();
        }
    }

    //do not change below
    RenderTexture renderTexture;
     int lastWidht = 0, lastHeight = 0;
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (renderTexture == null || source.width != lastWidht || source.height != lastHeight)
        {

            lastWidht = source.width;
            lastHeight = source.height;
            // destroyBuffer(hitStack);
            hitStack = new ComputeBuffer(lastWidht * lastHeight * 64, sizeof(int));
            shader.SetBuffer(kernelHandle, "hitStack", hitStack);
            if (renderTexture != null)
            {
                renderTexture.DiscardContents();
                renderTexture.Release();
            }
            renderTexture = new RenderTexture(source);
            renderTexture.enableRandomWrite = true;
            RenderTexture randomTexture = new RenderTexture(source);
            randomTexture.enableRandomWrite = true;
            randomTexture.Create();

            renderTexture.Create();
            shader.SetTexture(kernelHandle, "Result", renderTexture);
            shader.SetTexture(kernelHandle, "RandomTexture:", randomTexture);
            int width = source.width; int height = source.height;
            bf_base_hits = new ComputeBuffer(width * height, System.Runtime.InteropServices.Marshal.SizeOf(typeof(Hit)));
            List<RayState> raysStates = new List<RayState>();// (width * height * (raysDepth + 1));
            for(int i=0;i< width * height * (raysDepth );i++)
            {
                raysStates.Add(new RayState());
            }
            fromListToBuffer(ref raysStates, ref bf_rays);
            shader.SetBuffer(kernelHandle, "bf_rays", bf_rays);
            destroyBuffer(bf_current_deep);
            bf_current_deep = new ComputeBuffer(width * height, sizeof(int));
            currentDeeps = new int[width * height];
            shader.SetBuffer(kernelHandle, "bf_current_deep", bf_current_deep);
            destroyBuffer(debug);
            debug = new ComputeBuffer(width * height, sizeof(int));
            debugData = new int[width * height]; ;
            shader.SetBuffer(kernelHandle, "debug", debug);

            print("camera size  updated");

        }
        if (renderTexture.IsCreated())
        {
            ComputeRender(renderTexture, lastWidht, lastHeight);
            Graphics.Blit(renderTexture, destination);
        }
    }


    void fromListToBuffer<T>(ref List<T> list, ref ComputeBuffer buffer)
    {
        int count = list.Count;
        count += 2 + count % 2;
        if (buffer != null)
            buffer.Dispose();
        buffer = new ComputeBuffer(count, System.Runtime.InteropServices.Marshal.SizeOf(typeof(T)), ComputeBufferType.Default);
        buffer.SetData(list.ToArray());
        destroyBuffer(bf_base_hits);
    }
    ComputeBuffer bf_base_hits;
    Transform lastCameraPos;
    bool UpdateCamera(int width, int height)
    {
        Camera camera = GetComponent<Camera>();
        if (camera.transform.hasChanged)
        {
            camera.transform.hasChanged = false;
            CameraStruct cameraStruct = new CameraStruct();
            cameraData = new CameraStruct[2];
            if (bf_camera != null)
            {
                bf_camera.Dispose();
            }
            bf_camera = new ComputeBuffer(2, System.Runtime.InteropServices.Marshal.SizeOf(typeof(CameraStruct)));
            Vector4 cameraPos = camera.transform.position;
            cameraPos.w = 1;
            cameraStruct.position = cameraPos;
            cameraStruct.baseTransform = Matrix4x4.TRS(Vector3.zero, camera.transform.rotation, new Vector3(1, 1, 1));

            cameraStruct.near = camera.nearClipPlane;
            cameraStruct.far = camera.farClipPlane;

            cameraStruct.top = Mathf.Tan((camera.fieldOfView * .5f) * Mathf.PI / 180f);//top com z = 1;
            if (cameraStruct.top < 0) cameraStruct.top *= -1;
            cameraStruct.bottom = -cameraStruct.top; // bottom com z= 1;
            cameraStruct.right = cameraStruct.top * camera.aspect;
            cameraStruct.left = -cameraStruct.right;
            cameraStruct.width = width;
            cameraStruct.height = height;
            cameraStruct.backgroundColor = new Vector4(camera.backgroundColor.r,
                camera.backgroundColor.g,
                camera.backgroundColor.b,
                1);
            cameraStruct.backgroundColor.w = 1;
            cameraData[0] = cameraStruct;
            cameraData[1] = cameraStruct;
            bf_camera.SetData(cameraData);
            shader.SetBuffer(kernelHandle, "cameraBuffer", bf_camera);


            return true;
        }
        return false;
    }
    bool resetRender = false;
    public void ResetRender()
    {
        resetRender = true;
        // print("resetted render");
    }

    public void Dispose()
    {
        Dispose(true);

    }
    protected virtual void Dispose(bool disp)
    {
        //Dispose();
        destroyBuffer(bf_camera);
        destroyBuffer(bf_meshdata);
        destroyBuffer(bf_meshtransform);
        destroyBuffer(bf_normals);
        destroyBuffer(bf_triangles);
        destroyBuffer(bf_vertices);
        destroyBuffer(bf_boundingboxes);
        destroyBuffer(hitStack);
        destroyBuffer(bf_materials);
        //destroyBuffer(bf_randomInts);
        destroyBuffer(bf_lights);
        destroyBuffer(bf_rays);
        //   destroyBuffer(bf_pixel_stream);
        destroyBuffer(bf_base_hits);
        destroyBuffer(debug);
        destroyBuffer(bf_current_deep);
    }
}
