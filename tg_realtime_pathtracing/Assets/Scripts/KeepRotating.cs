﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepRotating : MonoBehaviour {
    public Vector3 speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Quaternion rotation = transform.rotation;
     
        rotation.eulerAngles =rotation.eulerAngles+ Time.deltaTime*speed;
        transform.rotation = rotation;
	}
}
