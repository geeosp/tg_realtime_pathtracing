﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Classe pra testar o movimento de objetos;
 * Ele fica movendo em um circulo
 * 
 */
public class GeoTransform : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    float angle;
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(Mathf.Sin(angle+=Time.deltaTime), Mathf.Cos(angle));
		
	}
}
