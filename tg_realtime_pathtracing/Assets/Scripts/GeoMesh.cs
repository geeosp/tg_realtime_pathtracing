﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GeoStructs;

/**
 * Todoss os objetos que devem ser renderizados devem ter esse componente.
 * É a interface que trabalha com o GeoRenderer.class
 */
public class GeoMesh : MonoBehaviour
{
    public MeshTransform meshTransform;
    public MeshData meshData;
    public static int MAX_TRIANGLES_PER_BOX = 2;
    //o mesh  constante do objeto
    Mesh mesh;
    public int triangleCount;
    //  public BoxMinMax box;
    public Color color
    {
        get
        {
            return material.color;

        }
    }
    public bool isLight(){
        return material.emission>0;
    }

    List<BoxMinMax> boundingBoxes;
    public int boxCount;
    BoxMinMax[] BoundingBoxTree;
    public BoxMinMax[] getBoundBoxTree()
    {
        return this.BoundingBoxTree;
    }
    public int modelIndex;
public  GeoMaterial material;
    // Use this for initialization
    void Start()
    {
        boundingBoxes = new List<BoxMinMax>();
        mesh = GetComponent<MeshFilter>().mesh;
        triangleCount = mesh.triangles.Length / 3;
        //    generateBoundingBoxes();

       
        Renderer renderer = GetComponent<Renderer>();
        material.color = renderer.material.GetColor("_Color");   
        material.diffuse = renderer.material.GetFloat("_Diffuse");
        material.specular = renderer.material.GetFloat("_Metallic");
        material.transmitted = renderer.material.GetFloat("_Transmitted");
        material.emission = renderer.material.GetFloat("_Emission");
        material.specularCoef = renderer.material.GetFloat("_Glossiness");
        material.refractionCoeficient = renderer.material.GetFloat("_RefractionCoeficient");


        GeoRenderer georenderer = GeoRenderer.getInstance();
        if (georenderer != null)
        {
            //avisa ao GeoRenderer que a quantidade de meshs foi alterada e que ele deverá atualizar os buffers da placa de video;
           //georenderer.UpdateMeshs();
        }


    }
    public Mesh getMesh()
    {
        return mesh;

    }
    struct IntVector3
    {
        public int x, y, z;
        public IntVector3(int[] xyz)
        {
            x = xyz[0];
            y = xyz[1];
            z = xyz[2];
        }
    }
    class Triangle
    {
        public IntVector3 vertices;
        public bool used;
        public Triangle(IntVector3 t, bool used)
        {
            this.used = used;
            this.vertices = t;
        }
    }
    /**
     * O objetivo dessa funcao é reorganizar os triangulos de modo que no array de trangulos aqueles que forem adjacentes fiquem proximos
     * 
     */
    public void generateBoundingBoxes()
    {
        Matrix4x4 localtoworld = GetComponent<MeshFilter>().gameObject.transform.localToWorldMatrix;

        //uma traduçao do mesh.triangles pra estrutura que a gene ta usando
        List<Triangle> origintriangles = new List<Triangle>();
        //esse é o resultado. origin triangles reorganizado de modo que os triangulos adjacentes apareçam proximo na lista
        List<Triangle> finalList = new List<Triangle>();
        //estrutura auxiliar que representa o grafo. cada indice representa um vertica, em cada vertica, uma lista dos triangulos que o contem
        
        mesh = GetComponent<MeshFilter>().mesh;
        List<Triangle>[] adjacencesToVertex = new List<Triangle>[mesh.vertices.Length];
        //estrutura auxiliar no processo de reorganizaçao, aqui estao os triangulos proximos daqueles que ja processmos
        Queue<Triangle> alcancaveis = new Queue<Triangle>();
        boundingBoxes = new List<BoxMinMax>();
        //inicializaçao do grafo
        for (int i = 0; i < adjacencesToVertex.Length; i++)
        {
            adjacencesToVertex[i] = new List<Triangle>();
        }
        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            Triangle triang = new Triangle(new IntVector3(new int[] {
                    mesh.triangles[i],
                    mesh.triangles[i+1],
                    mesh.triangles[i+2]
                }), false);
            origintriangles.Add(triang);
            adjacencesToVertex[triang.vertices.x].Add(triang);
            adjacencesToVertex[triang.vertices.y].Add(triang);
            adjacencesToVertex[triang.vertices.z].Add(triang);
        }
        //numero de triangulos processados
        int processed = 0;




        alcancaveis.Enqueue(adjacencesToVertex[0][0]);
        //everything initialized

        while (processed != origintriangles.Count)
        {
            int count = 0;
            BoxMinMax box = new BoxMinMax(0);
            box.t_init = processed;
            while (count < Mathf.Max(16, MAX_TRIANGLES_PER_BOX) && alcancaveis.Count != 0)
            {
                Triangle a = alcancaveis.Dequeue();
                if (!a.used)
                {
                    a.used = true;
                    foreach (Triangle t in adjacencesToVertex[a.vertices.x])
                    {
                        if (!t.used)
                        {
                            alcancaveis.Enqueue(t);
                        }
                    }
                    foreach (Triangle t in adjacencesToVertex[a.vertices.y])
                    {
                        if (!t.used)
                        {
                            alcancaveis.Enqueue(t);
                        }
                    }
                    foreach (Triangle t in adjacencesToVertex[a.vertices.z])
                    {
                        if (!t.used)
                        {
                            alcancaveis.Enqueue(t);
                        }
                    }

                    box.min = Vector4.Min(box.min, mesh.vertices[a.vertices.x]);
                    box.max = Vector4.Max(box.max, mesh.vertices[a.vertices.x]);

                    box.min = Vector4.Min(box.min, mesh.vertices[a.vertices.y]);
                    box.max = Vector4.Max(box.max, mesh.vertices[a.vertices.y]);
                    box.min = Vector4.Min(box.min, mesh.vertices[a.vertices.z]);
                    box.max = Vector4.Max(box.max, mesh.vertices[a.vertices.z]);

                    finalList.Add(a);
                    count++;
                    box.t_count = count;
                    processed++;

                }
            }
            boundingBoxes.Add(box);
            if (alcancaveis.Count > 0)
            {
                // Triangle ttmp = null;
                while (alcancaveis.Count != 0 && alcancaveis.Peek().used)
                {
                    alcancaveis.Dequeue();
                }
                if (alcancaveis.Count != 0)
                {
                    Triangle ttmp = alcancaveis.Peek();
                    alcancaveis.Clear();
                    alcancaveis.Enqueue(ttmp);

                }
            }

            if (alcancaveis.Count == 0)
            {
                bool found = false;
                for (int k = 0; !found && k < origintriangles.Count; k++)
                {
                    Triangle t = origintriangles[k];
                    if (!t.used)
                    {
                        alcancaveis.Enqueue(t);
                        found = true;
                    }
                }
            }
        }
        //Se isso for true, 
        //Aqui os triangulos estao agrupados por boxes de tamanho min de 16 triangulos por box;
        //Fiz isso para que eles nao ficassem muitos esparços
        if (MAX_TRIANGLES_PER_BOX < 16)
        {




        }





        boxCount = boundingBoxes.Count;



        int x = 1;
        while (x < boxCount)
        {
            x *= 2;
        }
        BoxMinMax lastBox = new BoxMinMax();
        lastBox.max = boundingBoxes[boxCount - 1].max;
        lastBox.min = boundingBoxes[boxCount - 1].min;
        lastBox.t_init = boundingBoxes[boxCount - 1].t_init;
        lastBox.t_count = 0;
        for (int k = boxCount; k <= x; k++)
        {

            boundingBoxes.Add(lastBox);
        }

        BoundingBoxTree = new BoxMinMax[2 * x];

        for (int k = x - 1, i = 0; i < boundingBoxes.Count; i++)
        {
            BoundingBoxTree[k + i] = boundingBoxes[i];
        }
        for (int i = x - 2; i >= 0; i--)
        {
            BoxMinMax b = new BoxMinMax();
            int ind1, ind2;
            ind1 = 2 * i + 1;
            ind2 = 2 * i + 2;
            b.max = Vector3.Max(BoundingBoxTree[ind1].max, BoundingBoxTree[ind2].max);
            b.min = Vector3.Min(BoundingBoxTree[ind1].min, BoundingBoxTree[ind2].min);
            b.t_init = BoundingBoxTree[ind1].t_init;
            b.t_count = BoundingBoxTree[ind1].t_count + BoundingBoxTree[ind2].t_count;
            Vector4 inverseScale = transform.lossyScale;
            /*
            for (int d=0;d< 3; d++)
            {
                inverseScale[d] = 1 / inverseScale[d];
            }
            b.max = Vector4.Scale(b.max, inverseScale);
            b.min = Vector4.Scale(b.min, inverseScale);
    */
            BoundingBoxTree[i] = b;
        }






        int[] newTriangles = new int[finalList.Count * 3];


        for (int i = 0; i < finalList.Count; i++)
        {
            Triangle t = finalList[i];
            newTriangles[3 * i] = t.vertices.x;
            newTriangles[3 * i + 1] = t.vertices.y;
            newTriangles[3 * i + 2] = t.vertices.z;
        }
        mesh.triangles = newTriangles;


    }



    void Update()
    {
        if (transform.hasChanged)
        {
            transform.hasChanged = false;
            GeoRenderer.getInstance().ResetRender();
        }
        //atualiza o bounding box do objeto
     
        if (Input.GetKeyDown(KeyCode.Space))
        {
            generateBoundingBoxes();
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            gizmosBoxtoShow++;
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            gizmosBoxtoShow--;
        }
        Renderer renderer = GetComponent<Renderer>();
        material.color = renderer.material.GetColor("_Color");
        material.diffuse = renderer.material.GetFloat("_Diffuse");
        material.specular = renderer.material.GetFloat("_Metallic");
        material.transmitted = renderer.material.GetFloat("_Transmitted");
        material.emission = renderer.material.GetFloat("_Emission");
        material.specularCoef = renderer.material.GetFloat("_Glossiness");
        material.refractionCoeficient = renderer.material.GetFloat("_RefractionCoeficient");

    }




    private void OnDestroy()
    {
        GeoRenderer renderer = GeoRenderer.getInstance();
        if (renderer != null)
        {
            renderer.UpdateMeshs();
        }

    }

    public Color getDifuseColor()
    {

        return GetComponent<Material>().color;

    }




    //desenha o bounding box  circle quando os quizmos estao selecionados.
    public int gizmosBoxtoShow = 0;

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        // Gizmos.color.a = .1f;

        Gizmos.matrix = transform.localToWorldMatrix;

        if (BoundingBoxTree != null)
            for (int k = (int)Mathf.Max(0, Mathf.Pow(2, gizmosBoxtoShow) - 1), i = 0; i <Mathf.Min(Mathf.Log(BoundingBoxTree.Length, 2), Mathf.Pow(2, gizmosBoxtoShow)); i++)

            {
                BoxMinMax b = BoundingBoxTree[k + i];
                Vector4 c = .5f * (b.min + b.max);// + (Vector4)transform.position;
                Gizmos.DrawWireCube(c, (b.max - b.min));
            }

        Gizmos.color = Color.magenta;
        if(boundingBoxes!=null)
        foreach (BoxMinMax b in boundingBoxes)

        {
            Vector4 c = .5f * (b.min + b.max);//+ (Vector4)transform.position;
            Gizmos.DrawWireCube(c, (b.max - b.min));
        }


        Gizmos.color = Color.red;


    }



}
