﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class InvisibleCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {
		Camera cam = GetComponent<Camera>();
		
		cam.cullingMask = 0;
	}
}
